package util;

import java.io.Serializable;
import java.net.InetAddress;

public class MensagemUDP implements Serializable{

	private static final long serialVersionUID = 1L;
	private Object mensagem;
	private InetAddress destinoIP;
	private int destinoPorta;
	private String cabecalho;
	
	
        /**
         * Construtor da classe
         * 
         * @param mensagem - A mensagem a ser enviada.
         * @param destinoIP - O IP de destino.
         * @param destinoPorta - A porta de destino.
         * @param cabecalho - Um atributo adicional para especificar a semântica da mensagem
         */
	public MensagemUDP(Object mensagem, InetAddress destinoIP,int destinoPorta,String cabecalho){
		this.mensagem = mensagem;
		this.destinoIP = destinoIP;
		this.destinoPorta = destinoPorta;
		this.setCabecalho(cabecalho);
	}
	
	public Object getMensagem() {
		return mensagem;
	}
	public void setMensagem(Object mensagem) {
		this.mensagem = mensagem;
	}
	public InetAddress getDestinoIP() {
		return destinoIP;
	}
	public void setDestinoIP(InetAddress destinoIP) {
		this.destinoIP = destinoIP;
	}
	public int getDestinoPorta() {
		return destinoPorta;
	}
	public void setDestinoPorta(int destinoPorta) {
		this.destinoPorta = destinoPorta;
	}

	public String getCabecalho() {
		return cabecalho;
	}

	public void setCabecalho(String cabecalho) {
		this.cabecalho = cabecalho;
	}



	
}
