package view;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import controller.SensorController;
import exceptions.ServidorNaoRespondeuException;
import java.awt.Color;
import java.awt.Font;

public class PacienteView extends JFrame implements Observer{

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private SensorController controller;
	private JSlider batimentos;
	private JSlider pressaoSistolica;
	private JSlider pressaoDiastolica;
	private JCheckBox emRepouso;
	private JTextPane diastolicaPane;
	private JTextPane sistolicaPane;
	private JTextPane batimentosPane;
	private String nomePaciente;
	private String cpf;
	private String enderecoIP;
	private JTextPane alerta;
	private int porta;

	/**
	 * Create the frame.
	 * @throws UnsupportedLookAndFeelException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws ClassNotFoundException 
	 */
	public PacienteView(String nomePaciente, String cpf,String enderecoIP, String textoPorta) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		
		//Configuração do endereço
		this.nomePaciente = nomePaciente;		
		this.enderecoIP = enderecoIP;
		this.cpf = cpf;
		this.porta = Integer.parseInt(textoPorta);

		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 280, 335);
		setTitle(nomePaciente+"- @"+enderecoIP+":"+porta);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		

		try {
			controller = new SensorController(nomePaciente,cpf,enderecoIP,porta);
			controller.addObserver(this);
		} catch (ServidorNaoRespondeuException e1) {
			JOptionPane.showMessageDialog(null,"O servidor não respondeu à solicitação de conexão");
			System.exit(1);
		}
		//Inicialização Batimentos
		JTextPane tituloBatimentos = new JTextPane();
		tituloBatimentos.setFont(new Font("Roboto", Font.PLAIN, 14));
		tituloBatimentos.setEditable(false);
		tituloBatimentos.setText("Batimentos");
		tituloBatimentos.setBounds(10, 26, 92, 20);
		tituloBatimentos.setBackground(new Color(255, 255, 255));
		contentPane.add(tituloBatimentos);
		
		batimentos = new JSlider();
		batimentos.setBackground(new Color(255, 255, 255));
		batimentos.setValue(100);
		batimentos.setMaximum(250);
		batimentos.setBounds(10, 56, 200, 26);
		contentPane.add(batimentos);
		
		batimentosPane = new JTextPane();
		batimentosPane.setFont(new Font("Roboto", Font.PLAIN, 14));
		batimentosPane.setBackground(new Color(255, 255, 255));
		batimentosPane.setText(Integer.toString(batimentos.getValue()));
		batimentosPane.setEditable(false);
		batimentosPane.setBounds(220, 56, 34, 20);
		contentPane.add(batimentosPane);
		
		
		//Inicialização Pressão Sistólica
		JTextPane txtpnPressoSistlica = new JTextPane();
		txtpnPressoSistlica.setFont(new Font("Roboto", Font.PLAIN, 14));
		txtpnPressoSistlica.setText("Press\u00E3o Sist\u00F3lica");
		txtpnPressoSistlica.setBounds(10, 93, 130, 20);
		txtpnPressoSistlica.setBackground(new Color(255, 255, 255));
		contentPane.add(txtpnPressoSistlica);
		
		pressaoSistolica = new JSlider();
		pressaoSistolica.setBackground(new Color(255, 255, 255));
		pressaoSistolica.setValue(12);
		pressaoSistolica.setMaximum(40);
		pressaoSistolica.setBounds(10, 124, 200, 26);
		contentPane.add(pressaoSistolica);
		
		sistolicaPane = new JTextPane();
		sistolicaPane.setFont(new Font("Roboto", Font.PLAIN, 14));
		sistolicaPane.setEditable(false);
		sistolicaPane.setBackground(new Color(255, 255, 255));
		sistolicaPane.setText(Integer.toString(pressaoSistolica.getValue()));
		sistolicaPane.setBounds(220, 124, 34, 20);
		contentPane.add(sistolicaPane);
		
		//Inicialização Pressão Diastólica
		JTextPane tituloPressaoDiastolica = new JTextPane();
		tituloPressaoDiastolica.setFont(new Font("Roboto", Font.PLAIN, 14));
		tituloPressaoDiastolica.setBackground(new Color(255, 255, 255));
		tituloPressaoDiastolica.setEditable(false);
		tituloPressaoDiastolica.setText("Press\u00E3o Diast\u00F3lica");
		tituloPressaoDiastolica.setBounds(10, 161, 130, 20);
		contentPane.add(tituloPressaoDiastolica);
		
		pressaoDiastolica = new JSlider();
		pressaoDiastolica.setBackground(new Color(255, 255, 255));
		pressaoDiastolica.setMaximum(40);
		pressaoDiastolica.setValue(8);
		pressaoDiastolica.setBounds(10, 192, 200, 26);
		contentPane.add(pressaoDiastolica);
		
		diastolicaPane = new JTextPane();
		diastolicaPane.setFont(new Font("Roboto", Font.PLAIN, 14));
		diastolicaPane.setBounds(220, 192, 34, 20);
		diastolicaPane.setBackground(new Color(255, 255, 255));
		diastolicaPane.setText(Integer.toString(pressaoDiastolica.getValue()));
		contentPane.add(diastolicaPane);
		
		//Checkbox repouso
		emRepouso = new JCheckBox("Em repouso?");
		emRepouso.setBackground(new Color(255, 255, 255));
		emRepouso.setFont(new Font("Roboto", Font.PLAIN, 14));
		emRepouso.setBounds(10, 225, 130, 23);
		contentPane.add(emRepouso);
		
		alerta = new JTextPane();
		alerta.setFont(new Font("Roboto", Font.PLAIN, 14));
		alerta.setText("");
		alerta.setForeground(Color.RED);
		alerta.setBounds(81, 255, 173, 20);
		alerta.setBackground(new Color(255, 255, 255));
		alerta.setEditable(false);
		contentPane.add(alerta);
		
		//Adição dos listeners
		batimentos.addChangeListener(new atualizarDados());
		pressaoSistolica.addChangeListener(new atualizarDados());
		pressaoDiastolica.addChangeListener(new atualizarDados());
		emRepouso.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				new atualizarDados().changeOnClick();
			}
		});
		
		setVisible(true);
		new atualizarDados().atualiza();
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if(o instanceof SensorController){
			if(controller.estaConectado()){
				alerta.setText("");
			}else{
				JOptionPane.showMessageDialog(null, "Conexão perdida");
				alerta.setText("Conexão perdida");
			}
		}	
	}
	
	private class atualizarDados implements ChangeListener{

		@Override
		public void stateChanged(ChangeEvent e) {
			atualiza();
		}
		
		public void changeOnClick(){
			atualiza();
		}
		
		public void atualiza(){
			int bpm = batimentos.getValue();
			int sistolica = pressaoSistolica.getValue();
			int diastolica = pressaoDiastolica.getValue();
			boolean repouso = emRepouso.isSelected();
			
			batimentosPane.setText(Integer.toString(bpm));
			diastolicaPane.setText(Integer.toString(diastolica));
			sistolicaPane.setText(Integer.toString(sistolica));
			
			controller.atualizarDados(bpm,sistolica,diastolica,repouso);
		}
		
	}

	
}
