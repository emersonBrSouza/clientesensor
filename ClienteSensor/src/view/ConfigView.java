package view;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.text.MaskFormatter;

public class ConfigView {

	private JFrame frmSensorLogin;
	private JTextField campoNome;
	private JTextField campoIP;
	private JTextField campoPorta;
	private JFormattedTextField campoCPF;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConfigView window = new ConfigView();
					window.frmSensorLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ConfigView() {
		try {
			initialize();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws UnsupportedLookAndFeelException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws ClassNotFoundException 
	 */
	private void initialize() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		 
		frmSensorLogin = new JFrame();
		frmSensorLogin.setResizable(false);
		frmSensorLogin.getContentPane().setBackground(new Color(102, 153, 204));
		frmSensorLogin.setTitle("Sensor - Login");
		frmSensorLogin.setBounds(100, 100, 328, 335);
		frmSensorLogin.getContentPane().setLayout(null);
		
		campoNome = new JTextField();
		campoNome.setBounds(38, 56, 247, 20);
		frmSensorLogin.getContentPane().add(campoNome);
		campoNome.setColumns(10);
		
		campoIP = new JTextField();
		campoIP.setBounds(38, 182, 139, 20);
		frmSensorLogin.getContentPane().add(campoIP);
		campoIP.setColumns(10);
		
		campoPorta = new JTextField();
		campoPorta.setBounds(199, 182, 86, 20);
		frmSensorLogin.getContentPane().add(campoPorta);
		campoPorta.setColumns(10);
		
		JTextPane txtpnNome = new JTextPane();
		txtpnNome.setFont(new Font("Roboto", Font.PLAIN, 14));
		txtpnNome.setEditable(false);
		txtpnNome.setText("Nome");
		txtpnNome.setBounds(38, 25, 89, 20);
		txtpnNome.setBackground(frmSensorLogin.getContentPane().getBackground());
		frmSensorLogin.getContentPane().add(txtpnNome);
		
		JTextPane txtpnEndereoIp = new JTextPane();
		txtpnEndereoIp.setFont(new Font("Roboto", Font.PLAIN, 14));
		txtpnEndereoIp.setEditable(false);
		txtpnEndereoIp.setText("Endere\u00E7o IP");
		txtpnEndereoIp.setBounds(38, 151, 89, 20);
		txtpnEndereoIp.setBackground(frmSensorLogin.getContentPane().getBackground());
		frmSensorLogin.getContentPane().add(txtpnEndereoIp);
		
		JTextPane txtpnPorta = new JTextPane();
		txtpnPorta.setFont(new Font("Roboto", Font.PLAIN, 14));
		txtpnPorta.setEditable(false);
		txtpnPorta.setText("Porta");
		txtpnPorta.setBounds(199, 151, 41, 20);
		txtpnPorta.setBackground(frmSensorLogin.getContentPane().getBackground());
		frmSensorLogin.getContentPane().add(txtpnPorta);
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.setFont(new Font("Roboto", Font.PLAIN, 14));
		btnEntrar.setBounds(105, 245, 89, 23);
		btnEntrar.addMouseListener(new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e) {
				if(!campoVazio(campoCPF.getText()) && !campoVazio(campoNome.getText()) && !campoVazio(campoIP.getText()) && !campoVazio(campoPorta.getText())){
					frmSensorLogin.dispose();
					try {
						new PacienteView(campoNome.getText(),campoCPF.getText(),campoIP.getText(),campoPorta.getText());
					} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
							| UnsupportedLookAndFeelException e1) {
						System.out.println("Erro no LookAndFeel");
					}
				}else{
					JOptionPane.showMessageDialog(null, "Preencha todos os campos!");
				}
			}
		});
		frmSensorLogin.getContentPane().add(btnEntrar);
		
		
		MaskFormatter cpfFormatter = null;
		try {
			cpfFormatter = new MaskFormatter("###.###.###-##");
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		campoCPF = new JFormattedTextField(cpfFormatter);
		campoCPF.setBounds(38, 120, 247, 20);
		frmSensorLogin.getContentPane().add(campoCPF);
		campoCPF.setColumns(10);
		
		JTextPane txtpnCpfDoPaciente = new JTextPane();
		txtpnCpfDoPaciente.setFont(new Font("Roboto", Font.PLAIN, 14));
		txtpnCpfDoPaciente.setBackground(frmSensorLogin.getContentPane().getBackground());
		txtpnCpfDoPaciente.setEditable(false);
		txtpnCpfDoPaciente.setText("CPF do Paciente");
		txtpnCpfDoPaciente.setBounds(38, 89, 139, 20);
		frmSensorLogin.getContentPane().add(txtpnCpfDoPaciente);
		
		frmSensorLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private boolean campoVazio(String texto){
		if(texto.isEmpty() || texto.trim().isEmpty() || campoCPF.getText().trim().length() < 10){
			return true;
		}else{
			return false;
		}	
	}
}
