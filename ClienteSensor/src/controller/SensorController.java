package controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Observable;

import exceptions.ServidorNaoRespondeuException;
import model.Paciente;
import model.Sensor;
import util.Acao;

public class SensorController extends Observable{

    private Sensor sensor;
    private String enderecoIPServidor;
    private int portaServidor;
    private boolean conectadoAoServidor = false;
    private Paciente paciente;
    private Thread transmitindo;


    /**
     * Construtor da classe
     * 
     * @param nome - O nome do cliente.
     * @param cpf - O cpf do cliente.
     * @param enderecoIPServidor - O endereço IP do servidor
     * @param porta - A porta do Servidor
     */
    public SensorController(String nome, String cpf,String enderecoIPServidor, int portaServidor) throws ServidorNaoRespondeuException{
        sensor = new Sensor(cpf);
        this.enderecoIPServidor = enderecoIPServidor;
        this.portaServidor = portaServidor;
        this.paciente = new Paciente(nome,sensor);

        conectar();
        transmitir();
    }

    /**
     * Conecta o cliente ao servidor.
     */
    public void conectar() {

        try {

            Socket clienteSocket = new Socket(enderecoIPServidor,portaServidor);

            clienteSocket.setSoTimeout(8000); //Define o tempo limite da resposta
            OutputStream saida = clienteSocket.getOutputStream(); // Obtém o canal de saída
            saida.write(serializarMensagens(new Acao("--connect",this.sensor.getId()))); //Envia a solicitação de conexão ao servidor
            saida.flush();

            InputStream entrada = new ObjectInputStream(clienteSocket.getInputStream()); // Obtém o canal de entrada
            Object resposta = ((ObjectInputStream) entrada).readObject();

            if(resposta instanceof String){
                if(resposta.equals("accepted")){
                    conectadoAoServidor = true;
                }
            }
            saida.close(); //Fecha o canal
            clienteSocket.close();//Encerra o socket

        } catch (SocketTimeoutException e){
            conectadoAoServidor = false;
        } catch (UnknownHostException e) {
            conectadoAoServidor = false;
            System.err.println("Servidor Desconhecido");
        } catch (IOException e) {
            conectadoAoServidor = false;
            System.err.println("Erro no envio/recebimento dos dados");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Envia os dados do sensor para o servidor usando UDP
     */
    public void enviar(){
        byte[] dados = serializarMensagens(new Acao("--save",this.paciente)); // Instancia a mensagem

        try {
            DatagramSocket socketSensor = new DatagramSocket(); 
            DatagramPacket pacote = new DatagramPacket(dados,dados.length,InetAddress.getByName(enderecoIPServidor),portaServidor); //Configura o pacote
            socketSensor.send(pacote); //Envia o pacote
            socketSensor.close();
        } catch (UnknownHostException e) {
            System.err.println("Servidor não encontrado");
        } catch (SocketException e) {
            System.err.println("Erro no canal de comunicação");
        } catch (IOException e) {
            System.err.println("Erro no envio dos dados");
        }
    }

    /**
     * Verifica periodicamente o estado da conexão entre o cliente e o servidor.
     */
    public void verificarConexao(){
        new Thread(){
            public void run(){
                try {
                    while(true){
                        sleep(3000); // Espera três segundos para reenviar o ping
                        Socket clienteSocket = new Socket(enderecoIPServidor,portaServidor);

                        OutputStream saida = clienteSocket.getOutputStream(); // Obtém o canal de saída
                        clienteSocket.setSoTimeout(8000); //Define o tempo limite de resposta do servidor.
                        saida.write(serializarMensagens(new Acao("--ping",sensor.getId()))); // "Pinga o servidor"
                        saida.flush();

                        InputStream entrada = new ObjectInputStream(clienteSocket.getInputStream());
                        Object resposta = ((ObjectInputStream) entrada).readObject();

                        if(resposta instanceof String){
                            if(resposta.equals("ok")){ // Verifica a resposta do servidor
                                conectadoAoServidor = true;
                                setChanged();
                                notifyObservers(); //Notifica aos observadores que a conexão está estabelecida
                            }
                        }
                        saida.close();
                        clienteSocket.close();

                    }	
                } catch(SocketTimeoutException e){
                        conectadoAoServidor = false;
                } catch(SocketException e){
                        conectadoAoServidor = false;
                } catch (InterruptedException | IOException | ClassNotFoundException e) {
                        e.printStackTrace();
                }
            }
        }.start();
    }

    /**
     * Transmite periodicamente os dados caso uma conexão esteja ativa.
     * 
     */
    public void transmitir(){
        transmitindo = new Thread(){
            public void run(){
                verificarConexao();
                while(true){
                    try {
                        sleep(2000); // Espera dois segundos para enviar um novo dado.
                        if(conectadoAoServidor){ // Verifica o estado da conexão
                            enviar();
                        }else{
                            setChanged(); 
                            notifyObservers();//Informa aos observadores que houve uma perda de conexão
                            verificarConexao();
                        }
                    } catch (InterruptedException e) {
                        System.err.println("O sistema foi interrompido inesperadamente");
                    }
                }
            }
        };
        transmitindo.start();	
    }

    
    /**
     * Transforma um objeto em um array de bytes
     * 
     * @param mensagem - O objeto a ser transformado.
     * @return byte[] - O array de bytes.
     */
    public byte[] serializarMensagens(Object mensagem){
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        try {
                //Converte o objeto em um array de bytes
                ObjectOutput out = new ObjectOutputStream(b);
                out.writeObject(mensagem);
                out.flush();
                return b.toByteArray();
        } catch (IOException e) {
                System.err.println("Erro no envio/recebimento dos dados");
        }
        return null;

    }

    /**
     * Atualiza localmente os dados do sensor
     * 
     * @param batimentos - Os batimentos do paciente
     * @param sistolica - A pressão sistólica do paciente.
     * @param diastolica - A pressão diastólica do paciente.
     * @param emRepouso - O estado da movimentação do paciente.
     */
    public void atualizarDados(int batimentos,int sistolica,int diastolica,boolean emRepouso){
        this.sensor.setBatimentos(batimentos);
        this.sensor.setPressaoSistolica(sistolica);
        this.sensor.setPressaoDiastolica(diastolica);
        this.sensor.setEmRepouso(emRepouso);
    }

    /**
     * Retorna o estado da conexão
     * 
     * @return true - Se o paciente estiver conectado.
     * @return false - Se o paciente estiver desconectado.
     */
    public boolean estaConectado() {
            return this.conectadoAoServidor;
    }
}
